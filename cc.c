
#include <x32.h>
#include <ucos.h>

#define INCREMENT_STEP  4
#define DECREMENT_STEP  4
#define MAX_THROTTLE    1024
#define ERROR_LED       7
#define TASK_STACK_SIZE 1024

#define BUTTON1         (1 << 0)
#define BUTTON2         (1 << 1)
#define BUTTON3         (1 << 2)
#define BUTTON4         (1 << 3)

unsigned throttle;
unsigned setpoint;
int speed;

int new_a, new_b;
int state_a, state_b;
int previous_count;

int interrupt_disable;
int prev_button;

OS_EVENT *error_sem;
OS_EVENT *increment_sem;
OS_EVENT *decrement_sem;
OS_EVENT *reset_sem;
OS_EVENT *engage_sem;
UBYTE err;

/* actions */

void reset()
{
	/* this shuts down the motor, resets the SSD and quits the program. */
	peripherals[PERIPHERAL_LEDS] = 0;
	peripherals[PERIPHERAL_DISPLAY] = 0;
	peripherals[PERIPHERAL_DPC1_WIDTH] = 0;
	throttle = 0;
	exit();
}

void engage()
{
	/* will enable the cruise control mode. */
	/* "setpoint" will equal "speed" */
}

void disengage()
{
	/* will disable the cruise control mode */
}

void decrement()
{
	/* lower "throttle" or "setpoint", when cruise control mode is
	 * enabled */

	if (throttle <= DECREMENT_STEP)
		throttle = 0;
	else
		throttle -= DECREMENT_STEP;
}

void increment()
{
	/* increase "trottle" or "setpoint" when cruise control mode is
	 * enabled */

	throttle += INCREMENT_STEP;

	if (throttle > MAX_THROTTLE)
		throttle = MAX_THROTTLE;
}

/* interrupts */

void handle_button(int bit, OS_EVENT *sem)
{
	int button;
	button = peripherals[PERIPHERAL_BUTTONS];

	/* the button must be active
	 * the button must be different compared to the previous state, so
	 * for example, 0100, then pressing a second button gives: 0110
	 * so now it's easy XOR for the correct bit
	 * finally it shouldn't post the semaphore if the task isn't executed yet */

	if (button & bit && (button ^ prev_button) & bit && !(interrupt_disable & bit)) {
		interrupt_disable |= bit;
		OSSemPost(sem);
	}
}

/* Interrupt handler for buttons */
void isr_buttons()
{

	OSIntEnter();

	handle_button(BUTTON1, increment_sem);
	handle_button(BUTTON2, decrement_sem);
	handle_button(BUTTON3, engage_sem);
	handle_button(BUTTON4, reset_sem);

	prev_button = peripherals[PERIPHERAL_BUTTONS];

	OSIntExit();
}

/* tasks */

void *task_write_throttle_stack[TASK_STACK_SIZE];
void *task_reset_stack[TASK_STACK_SIZE];
void *task_increment_stack[TASK_STACK_SIZE];
void *task_decrement_stack[TASK_STACK_SIZE];
void *task_engage_stack[TASK_STACK_SIZE];

/* periodical task which writes "throttle" to the SSD every uC/OS time slice */
void task_write_throttle(void *arg)
{
	int count;
	while (1) {

		/* write throttle to the engine */
		peripherals[PERIPHERAL_DPC1_WIDTH] = throttle;

		/* write throttle to SSD */
		count = peripherals[PERIPHERAL_ENGINE_DECODED];

		speed = count - previous_count;
		previous_count = count;

		/* speed left 2 digits, throttle right.
		 * throttle divided by 4, because 1024 / 4 = 256 = 16 * 16 */
		peripherals[PERIPHERAL_DISPLAY] = (speed << 8) | (throttle >> 2);

		/* 1 tick <= 20 ms */
		OSTimeDly(1);
	}
}

void handle_task(OS_EVENT *sem, int btn, void (*task)())
{
	while (1) {
		OSSemPend(sem, WAIT_FOREVER, &err);
		OSTimeDly(2);
		if (peripherals[PERIPHERAL_BUTTONS] & btn) {
			(*task)();
		}
		interrupt_disable &= ~btn;
	}
}

void task_increment(void *arg)
{
	handle_task(increment_sem, BUTTON1, increment);
}

void task_decrement(void *arg)
{
	handle_task(decrement_sem, BUTTON2, decrement);
}

void task_engage(void *arg)
{
	handle_task(engage_sem, BUTTON3, engage);
}

void task_reset(void *arg)
{
	handle_task(reset_sem, BUTTON4, reset);
}

void main()
{
	OSInit();

	OSTaskCreate(task_write_throttle, /* args */ 0, task_write_throttle_stack, /* priority */ 1);
	OSTaskCreate(task_reset, /* args */ 0, task_reset_stack, /* priority */ 2);
	OSTaskCreate(task_increment, /* args */ 0, task_increment_stack, /* priority */ 3);
	OSTaskCreate(task_decrement, /* args */ 0, task_decrement_stack, /* priority */ 4);
	OSTaskCreate(task_engage, /* args */ 0, task_engage_stack, /* priority */ 5);

	/* set button interrupt address & priority */
	SET_INTERRUPT_VECTOR(INTERRUPT_BUTTONS, isr_buttons);
	SET_INTERRUPT_PRIORITY(INTERRUPT_BUTTONS, 10);
	ENABLE_INTERRUPT(INTERRUPT_BUTTONS);

	peripherals[PERIPHERAL_DPC1_PERIOD] = MAX_THROTTLE;

	/* enable interrupt globally */
	ENABLE_INTERRUPT(INTERRUPT_GLOBAL);

	/* initialize variables */
	throttle = setpoint = speed = previous_count = 0;
	interrupt_disable = 0;
	prev_button = 0;

	/* initialize semaphores */
	reset_sem = OSSemCreate(0);
	increment_sem = OSSemCreate(0);
	decrement_sem = OSSemCreate(0);
	engage_sem = OSSemCreate(0);

	OSStart();
}
